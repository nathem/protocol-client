package net.nathem.websocket;

import org.json.simple.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws URISyntaxException, IOException {

        NathemWSC client = new NathemWSC("myClient", "localhost:8080", "mykey");

        Scanner scanner = new Scanner(System.in);
        while(true)
        {
            scanner.nextLine();
            client.sendRequest(new ProxyRequest("PING", client.getName(), new JSONObject()));
        }
    }
}
