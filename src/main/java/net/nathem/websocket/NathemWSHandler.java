package net.nathem.websocket;


import net.nathem.websocket.request.PingRequest;
import org.json.simple.JSONObject;

/**
 * Extends this class to create a new handler.
 *
 * Handlers must be registred to {@link NathemWSC} using {@link NathemWSC#registerHandler(String, Class)}
 * Exemple : myNathemWSC.registerHandler("TIME", TimeHandler.class)
 */
public abstract class NathemWSHandler {
    protected String uuid;
    protected NathemWSC client;
    protected String type;
    protected JSONObject data;

    public NathemWSHandler(NathemWSC client, String uuid, String type, JSONObject data) {
        this.uuid = uuid;
        this.client = client;
        this.type = type;
        this.data = data;
        this.send();
    }

    /**
     * Get the uuid of the request handled
     * @return the uuid of the request
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Handle the request
     * @param data the data of the received request
     * @return the response data
     */
    abstract protected JSONObject handle(JSONObject data);

    /**
     * Called when response sent
     */
    abstract protected void onResponseSent();


    private void send()
    {
        JSONObject response = this.handle(this.data);
        this.client.send(this.uuid, this.type, response);
        this.client.log(type +" response sent");
        this.onResponseSent();
    }



}
