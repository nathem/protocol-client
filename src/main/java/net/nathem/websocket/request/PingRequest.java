package net.nathem.websocket.request;


import net.nathem.websocket.NathemWSRequest;
import org.json.simple.JSONObject;

public class PingRequest extends NathemWSRequest{

    @Override
    public String getType() {
        return "PING";
    }

    @Override
    public JSONObject buildData() {
        return new JSONObject();
    }

    @Override
    public void onResponse(JSONObject data) {
    }
}
