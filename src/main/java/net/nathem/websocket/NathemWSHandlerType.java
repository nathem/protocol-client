package net.nathem.websocket;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.json.simple.JSONObject;

public class NathemWSHandlerType {

	
	
	private Class<? extends NathemWSHandler> handlerClass;
	private String name;
	
	
    public NathemWSHandlerType(String name, Class<? extends NathemWSHandler> handlerClass) {
		this.name = name;
		this.handlerClass = handlerClass;
	}


	public NathemWSHandler newHandler(NathemWSC client, String uuid, String type, JSONObject data)
    {
    	
			Constructor<? extends NathemWSHandler> constructor;
			try {
				constructor = this.handlerClass.getConstructor(NathemWSC.class, String.class, String.class, JSONObject.class);
				try {
					return constructor.newInstance(client, uuid, type, data);
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					e.printStackTrace();
				}
			} catch (NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
			
    	return null;
    	
    }
    

    public boolean isResponseOf(String type)
    {
        return type.equals(this.name);
    }
}
