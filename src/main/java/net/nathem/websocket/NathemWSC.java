package net.nathem.websocket;


import net.nathem.websocket.handler.AuthHandler;
import net.nathem.websocket.handler.PingHandler;
import net.nathem.websocket.request.PingRequest;
import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_10;
import org.java_websocket.handshake.ServerHandshake;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Represents a Nathem websockets client
 */
public class NathemWSC extends WebSocketClient{

    private String serverKey;
    private String name;
    private String host;
    private ArrayList<NathemWSHandlerType> registeredResponseTypes;
    private ArrayList<NathemWSRequest> waitingRequests;

    /**
     * Create and start a new Nathem websockets client
     * @param name the client's name
     * @param host the full hostname (with port) of the server to connect with
     * @param serverKey the server's security key
     */
    public NathemWSC(String name, String host, String serverKey) {

        super(URI.create("ws://"+host), new Draft_10());
        this.name = name;
        this.host = host;
        this.serverKey = serverKey;
        this.registeredResponseTypes = new ArrayList<NathemWSHandlerType>();
        this.waitingRequests = new ArrayList<NathemWSRequest>();

        // Default handlers
        this.registerHandler("AUTH", AuthHandler.class);
        this.registerHandler("PING", PingHandler.class);

        this.connect();


    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {

        this.log("Connected to "+host);
    }

    @Override
    public void onMessage(String message) {
        //this.log("Message : " + message);
        JSONParser parser = new JSONParser();
        JSONObject jsonData;

        try {
            jsonData = (JSONObject) parser.parse(message);
        } catch (ParseException e) {
            this.log("Error : Invalid message");
            return;
        }

        String type = (String) jsonData.get("type");
        String uuid = (String) jsonData.get("uuid");

        JSONObject data = null;
        if(jsonData.get("data") instanceof JSONObject)
        {
            data = (JSONObject) jsonData.get("data");
        }
        else data = new JSONObject();


        // Received responses
        for(NathemWSRequest request : this.waitingRequests)
        {
            if(request.getUuid().equals(uuid))
            {
                request.receive(data);
                return;
            }
        }

        // Received requests
        for(NathemWSHandlerType responseType : this.registeredResponseTypes)
        {
            if(responseType.isResponseOf(type))
            {
                responseType.newHandler(this, uuid, type, data);
            }
        }

    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        this.log("Disconnected ("+code+") : " +reason);

    }

    @Override
    public void onError(Exception ex) {
        this.log("Error : "+ex.getMessage());
    }

    public void log(String message)
    {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date)+" ["+this.name+"] "+message);
    }

    public void send(String uuid, String type, JSONObject data)
    {
        JSONObject request = new JSONObject();
        request.put("uuid", uuid);
        request.put("type", type);
        request.put("data", data);

        StringWriter out = new StringWriter();

        try {
            request.writeJSONString(out);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //this.log(out.toString());
        this.send(out.toString());
    }

    /**
     * Register a new handler
     * @param name the type name of the requests to handle
     * @param handlerClass the handler class
     */
    public void registerHandler(String name, Class<? extends NathemWSHandler> handlerClass)
    {
        this.registeredResponseTypes.add(new NathemWSHandlerType(name, handlerClass));
    }

    public String getServerKey() {
        return serverKey;
    }

    public String getName() {
        return name;
    }

    public String getHost() {
        return host;
    }

    /**
     * Send a {@link NathemWSRequest} to the server
     * @param request the {@link NathemWSRequest} to send
     * @return the {@link NathemWSRequest} sent
     */
    public NathemWSRequest sendRequest(NathemWSRequest request)
    {
        this.waitingRequests.add(request);
        request.setClient(this);
        request.send();
        return request;
    }

    public void removeFromWaitingRequests(NathemWSRequest request)
    {
        this.waitingRequests.remove(request);
    }

    public boolean isConnected()
    {
        return (this.getReadyState() == WebSocket.READYSTATE.OPEN);
    }
}
