package net.nathem.websocket;


import org.json.simple.JSONObject;

import java.util.UUID;

/**
 * Extends this class to create a request
 */
public abstract class NathemWSRequest {

    protected String uuid;
    protected boolean isWaiting;
    protected NathemWSC client;
    protected JSONObject responseData;

    public NathemWSRequest() {
        this.isWaiting = true;
        this.uuid = UUID.randomUUID().toString();
    }

    /**
     * Request type name (request identifier)
     * @return the request type name
     */
    abstract public String getType();

    /**
     * Add some data into the request
     * @return the data to send with the request
     */
    abstract public JSONObject buildData();

    /**
     * Called when the response is caught
     * @param data the response data
     */
    abstract public void onResponse(JSONObject data);

    public void send()
    {
        this.client.send(uuid, this.getType(), this.buildData());
        this.client.log(this.getType()+" request sent");
    }

    public void receive(JSONObject data)
    {
        this.isWaiting = false;
        this.responseData = data;
        this.client.removeFromWaitingRequests(this);
        this.client.log(this.getType() +" response received");
        this.onResponse(data);
    }

    /**
     * Get the uuid of the request handled
     * @return the uuid of the request handled
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Is the request waiting for response
     * @return true if the request wait for response
     */
    public boolean isWaiting() {
        return isWaiting;
    }

    /**
     * Get the response data if response received
     * @return the response data
     */
    public JSONObject getResponseData() {
        return responseData;
    }

    public void setClient(NathemWSC client) {
        this.client = client;
    }
}
