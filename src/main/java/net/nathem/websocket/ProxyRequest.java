package net.nathem.websocket;

import org.json.simple.JSONObject;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

public class ProxyRequest extends NathemWSRequest{

    private String type;
    private String client;
    private JSONObject data;

    public ProxyRequest(String type, String client, JSONObject data) {
        this.type = type;
        this.client = client;
        this.data = data;
    }

    @Override
    public String getType() {
        return "PROXY";
    }

    @Override
    public JSONObject buildData() {
        JSONObject json = new JSONObject();
        json.put("client", this.client);
        json.put("type", this.type);
        json.put("data", this.data);
        return json;
    }

    @Override
    public void onResponse(JSONObject data) {
    }
}
