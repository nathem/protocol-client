package net.nathem.websocket.handler;


import net.nathem.websocket.NathemWSC;
import net.nathem.websocket.NathemWSHandler;
import net.nathem.websocket.request.PingRequest;
import org.json.simple.JSONObject;

public class AuthHandler extends NathemWSHandler {

    public AuthHandler(NathemWSC client, String uuid, String type, JSONObject data) {
        super(client, uuid, type, data);
    }

    @Override
    protected JSONObject handle(JSONObject data) {

        JSONObject response = new JSONObject();
        response.put("key", this.client.getServerKey());
        response.put("name", this.client.getName());
        return response;
    }

    @Override
    protected void onResponseSent() {
        this.client.sendRequest(new PingRequest());
    }
}
