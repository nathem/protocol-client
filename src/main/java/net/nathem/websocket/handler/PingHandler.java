package net.nathem.websocket.handler;

import net.nathem.websocket.NathemWSC;
import net.nathem.websocket.NathemWSHandler;
import net.nathem.websocket.request.PingRequest;
import org.json.simple.JSONObject;

public class PingHandler extends NathemWSHandler {

    public PingHandler(NathemWSC client, String uuid, String type, JSONObject data) {
        super(client, uuid, type, data);
    }

    @Override
    protected JSONObject handle(JSONObject data) {
        JSONObject json = new JSONObject();
        json.put("success", true);
        return json;
    }

    @Override
    protected void onResponseSent() {

    }
}
